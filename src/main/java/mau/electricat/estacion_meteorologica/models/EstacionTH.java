package mau.electricat.estacion_meteorologica.models;

import java.text.*;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "estaciones_th")
public class EstacionTH {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "humedad")
    private int humedad;

    @Column(name = "temperatura")
    private double temperatura;
    
    @Column(name = "momento_medicion")
    private Date momentoMedicion;

    

    /**
     * Generic Constructor 
     */
    public EstacionTH() {
        System.out.println("constructor generico");
        setMomentoMedicion();
    }

    /**
     * @param humedad
     * @param temperatura
     */
    public EstacionTH(int humedad, double temperatura) {
        System.out.println("constructor humedad-temperatura");
        setHumedad(humedad);
        setTemperatura(temperatura);
        setMomentoMedicion();
    }

    /**
     * @param humedad
     * @param temperatura
     * @param momentoMedicion
     */
    public EstacionTH(int humedad, double temperatura, Date momentoMedicion) {
        System.out.println("construcor humedad-temperatura-datetime");
        this.setHumedad(humedad);
        this.setTemperatura(temperatura);;
        this.setMomentoMedicion(momentoMedicion);
    }



    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the humedad
     */
    public int getHumedad() {
        return humedad;
    }

    /**
     * @param humedad the humedad to set
     */
    public void setHumedad(int humedad) {
        this.humedad = humedad;
    }

    /**
     * @return the temperatura
     */
    public double getTemperatura() {
        return temperatura;
    }

    /**
     * @param temperatura the temperatura to set
     */
    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    /**
     * @return the momentoMedicion
     */
    public Date getMomentoMedicion() {
        return momentoMedicion;
    }

    /**
     * @param momentoMedicion the momentoMedicion to set
     */
    public void setMomentoMedicion(Date momentoMedicion) {
        this.momentoMedicion = momentoMedicion;
    }

    /**
     * set the current datetime and store in momentoMedicion
     */
    public void setMomentoMedicion() {
        //DateFormat dateFormat= new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss");
        this.momentoMedicion = new Date();
    }


}
