package mau.electricat.estacion_meteorologica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EstacionMeteorologicaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EstacionMeteorologicaApplication.class, args);
	}

}
