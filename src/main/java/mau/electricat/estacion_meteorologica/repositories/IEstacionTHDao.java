package mau.electricat.estacion_meteorologica.repositories;

import org.springframework.data.repository.CrudRepository;

import mau.electricat.estacion_meteorologica.models.EstacionTH;

public interface IEstacionTHDao extends CrudRepository<EstacionTH, Integer> {

}// end interface IEstacionTHDao
