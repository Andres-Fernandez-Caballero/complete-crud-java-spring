package mau.electricat.estacion_meteorologica.services;

import java.util.List;

public interface ICrudService <T> {
    
    T getById(int id);
    
    T create(T entity);
    
    T update(T entity);
    
    void delete(int id);
    
    List<T> getAll();
    
}//end interface ICrudService
    

