package mau.electricat.estacion_meteorologica.services;

import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import mau.electricat.estacion_meteorologica.models.EstacionTH;
import mau.electricat.estacion_meteorologica.repositories.IEstacionTHDao;

@Service
public class EstacionTHService implements ICrudService<EstacionTH> {

    @Autowired
    private IEstacionTHDao estacionTHDao;

    @Override
    public EstacionTH getById(int id) {
        var result = estacionTHDao.findById(id);
        return result.isPresent() ? result.get() : null;
    }

    @Override
    public EstacionTH create(EstacionTH entity) {
        EstacionTH createdEstacion = estacionTHDao.save(entity);
        return createdEstacion;
    }

    @Override
    public EstacionTH update(EstacionTH entity) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void delete(int id) {
        estacionTHDao.deleteById(id);
    }

    @Override
    public List<EstacionTH> getAll() {

        return (List<EstacionTH>) estacionTHDao.findAll();
    }

}// end class EstacionTHService