package mau.electricat.estacion_meteorologica.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mau.electricat.estacion_meteorologica.models.EstacionTH;
import mau.electricat.estacion_meteorologica.services.EstacionTHService;

@RestController
// @RequestMapping("/estacion_th")
public class EstacionTHController {

    @Autowired
    private EstacionTHService estacionTHService;

    @GetMapping("/{id}")
    public EstacionTH getById(@PathVariable("id") int id) {
        return estacionTHService.getById(id);
    }

    @GetMapping("/")
    public List<EstacionTH> getAll() {
        return estacionTHService.getAll();
    }

    @GetMapping("/dashboard")
    public String home() {
        return "dashboard";
    }

    @PostMapping("/")
    public String create(@RequestBody EstacionTH estacionTH) {
        var createdEstacion = estacionTHService.create(estacionTH);
        return "registro creado con exito id:" + createdEstacion.getId();
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        estacionTHService.delete(id);
        return "registro eliminado con exito";
    }

}
